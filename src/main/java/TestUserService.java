/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mynx
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("sample","password"));
        System.out.println(UserService.getUsers());
        User updateUser = new User("example","password");
        UserService.updateUser(3, updateUser);
        System.out.println(UserService.getUsers());
        UserService.delUser(updateUser);
        System.out.println(UserService.getUsers());
        UserService.delUser(1);
        System.out.println(UserService.getUsers());
    
    }
    
}
